#include <stdio.h>
#include <math.h>

#ifndef M_PI
#define M_PI 3.141592653589793 // default if pi is not supplied by math.h
#endif

#include "lib/common/vector.h"
#include "lib/common/abort.h"
#include "lib/common/fixed.h"
#include "lib/common/triangle.h"
#include "lib/render/bitmap.h"
#include "lib/render/rasterise.h"
#include "lib/render/surface.h"
#include "lib/render/shader.h"
#include "lib/render/camera.h"
#include "lib/render/window.h"



void basic_uvs(ct_ct_Triangle3D *tri) {
    tri->ua = 0;
    tri->va = 0;

    tri->ub = 1;
    tri->vb = 0;

    tri->uc = 0;
    tri->vc = 1;
}

int main() {
    ct_ca_aNamePush("main");

    ct_cv_Vector camPos = ct_cv_vVal(0, -22, 0);
    ct_cv_Vector camRot = ct_cv_vVal(-100 * M_PI / 180, 0, 0);
    ct_rc_Camera cam;
    ct_rc_initCam(&cam, &camPos, &camRot, M_PI / 1.8);

    FILE *texFP = fopen("texture.png", "r");
    if (!texFP) ct_ca_doAbort("couldn't open texture.png for reading");

    ct_rb_RGBBitmap *tex = ct_rb_pngLoadRGB(texFP, "texture.png");
    fclose(texFP);

    /*
    printf("\n\nrotation matrix: \n");
    ct_cm_printMat(cam->rotMat);
    printf("\n\npersp. matrix: \n");
    ct_cm_printMat(cam->persp);
    printf("\n\ncamera matrix: \n");
    ct_cm_printMat(cam->pointTransformer);
    printf("\ncamera position: \n%s\n\n", ct_cv_vRepr(cam->pos));
    */

    /*ct_ct_Triangle3D *tri_a = ct_ct_make(
        ct_cv_vNew( 2, -10, 18),
        ct_cv_vNew( 0, -6,  12),
        ct_cv_vNew(-2, -10, 18)
    );

    basic_uvs(tri_a);

    ct_ct_Triangle3D *tri_b = ct_ct_make(
        ct_cv_vNew(2,    4,   -12 ),
        ct_cv_vNew(1.5,  1,    1.5),
        ct_cv_vNew(3,   -5,    3)
    );
    
    basic_uvs(tri_b);

    ct_ct_Triangle3D *tri_c = ct_ct_make(
        ct_cv_vNew(-4, -7, -4),
        ct_cv_vNew(-4, -7,  6),
        ct_cv_vNew( 4,  5,  4)
    );

    basic_uvs(tri_c);

    ct_ct_Triangle3D *tri_d = ct_ct_make(
        ct_cv_vNew(-4, -7, -4),
        ct_cv_vNew( 4,  5, -6),
        ct_cv_vNew( 4,  5,  4)
    );

    basic_uvs(tri_d);*/

    ct_ct_Triangle3D tri_a;
    ct_ct_init(&tri_a, ct_cv_vVal(2, 10, 18), ct_cv_vVal(0, -6, 12), ct_cv_vVal(-2, 10, 18));
    basic_uvs(&tri_a);

    ct_ct_Triangle3D tri_b;
    ct_ct_init(&tri_b, ct_cv_vVal(2, 4, -12), ct_cv_vVal(1.5, 1, 1.5), ct_cv_vVal(3, -5, 3));
    basic_uvs(&tri_b);

    ct_ct_Triangle3D tri_c;
    ct_ct_init(&tri_c, ct_cv_vVal(-4, -7, -4), ct_cv_vVal(-4, -7, 6), ct_cv_vVal(4, 5, 4));
    basic_uvs(&tri_c);

    ct_ct_Triangle3D tri_d;
    ct_ct_init(&tri_d, ct_cv_vVal(-4, -7, -4), ct_cv_vVal(4, 5, 6), ct_cv_vVal(4, 5, 4));
    basic_uvs(&tri_d);

    ct_rh_Shader shaders[4];

    shaders[0].applier = ct_rh_normal_applier();
    shaders[0].source = ct_rh_texture_source(tex);

    ct_rs_Color mulColor = { 0.8, 0.3, 0.3 };

    shaders[1].applier = ct_rh_mul_applier();
    shaders[1].source = ct_rh_color_source(&mulColor);

    ct_rs_Color transpColor = { 0.2, 0.2, 0.7 };

    shaders[2].applier = ct_rh_blend_applier(0.3);
    shaders[2].source = ct_rh_color_source(&transpColor);

    shaders[3].applier = ct_rh_mul_applier();
    shaders[3].source = ct_rh_barycentric_source();
    
    ct_rh_Shader *shaderListA[1] = { shaders };
    ct_rh_Shader *shaderListB[2] = { shaders, shaders + 1 };
    ct_rh_Shader *shaderListC[3] = { shaders, shaders + 3, shaders + 2 };

    /*
    printf("  aa2: %s, ab2: %s, ac2: %s\n", ct_cv_vRepr(ttri_a->a), ct_cv_vRepr(ttri_a->b), ct_cv_vRepr(ttri_a->c));
    printf("  ba2: %s, bb2: %s, bc2: %s\n", ct_cv_vRepr(ttri_b->a), ct_cv_vRepr(ttri_b->b), ct_cv_vRepr(ttri_b->c));
    printf("  ca2: %s, cb2: %s, cc2: %s\n", ct_cv_vRepr(ttri_c->a), ct_cv_vRepr(ttri_c->b), ct_cv_vRepr(ttri_c->c));
    */

    // export PNG
    /*
    ct_rb_RGBBitmap *bitmap = ct_rb_newRGB(800, 600);
    ct_rr_RasterTarget *targ = ct_rt_bitmap_textureTarget(bitmap);
    
    FILE *fp = fopen("out.png", "w");
    if (!fp) ct_ca_doAbort("could not open out.png for writing");
    
    ct_rb_exportRGB(fp, bitmap);
    
    fclose(fp);
    */

    // show window
    ct_rw_SDLApp app;
    ct_rw_init(&app, "Clawthorn demo 2019.11.25", 800, 600, SDL_RENDERER_ACCELERATED, 0);
    
    ct_rs_Surface *surf = ct_rs_surf_new();
    ct_rs_surf_init_window(surf, &app);

    SDL_Event event;

    double moveStep = 0;

    unsigned char stop = 0;

    // render frames!
    while (1) {
        while (SDL_PollEvent(&event)) {
            switch (event.type) {
                case SDL_QUIT:
                    stop = 1;
                    break;

                default:
                    break;
            }

            if (stop) break;
        }

        if (stop) break;
        SDL_Delay(1000 / 60);

        // change camera
        cam.pos.y += FTOFX(sin(moveStep += M_PI / 18) * 2 / M_PI);
        if (moveStep > M_PI * 2) moveStep -= M_PI * 2;

        { // scope for rot
            ct_cv_Vector rot = ct_cv_vVal(0, 5 * FXPI / 180, 0);
            ct_rc_rotateByXYZ(&cam, &rot);
        }

        { // scope for rendering (ttri_*, etc)
            // re-render
            ct_ct_Triangle2D ttri_a;
            ct_ct_Triangle2D ttri_b;
            ct_ct_Triangle2D ttri_c;
            ct_ct_Triangle2D ttri_d;

            ct_rc_makePerspectiveTriangle(&ttri_a, &cam, &tri_a);
            ct_rc_makePerspectiveTriangle(&ttri_b, &cam, &tri_b);
            ct_rc_makePerspectiveTriangle(&ttri_c, &cam, &tri_c);
            ct_rc_makePerspectiveTriangle(&ttri_d, &cam, &tri_d);

            ct_rs_surf_clear(surf);

            // rasterize
            ct_rr_Rasterisation targ;
            ct_rr_init_raster(&targ, surf);

            ct_rr_rasterizeInto(&targ, &ttri_a);
            ct_rr_rasterizeInto(&targ, &ttri_b);
            ct_rr_rasterizeInto(&targ, &ttri_c);
            ct_rr_rasterizeInto(&targ, &ttri_d);

            // display
            ct_rw_rendererPresent(&app);
            ct_rr_deinit_raster(&targ);
        }
    }

    printf("\n\nDone.\n");

    // dealloc stuff

    ct_rh_free_source(shaders[0].source);
    ct_rh_free_source(shaders[1].source);
    ct_rh_free_source(shaders[2].source);
    ct_rh_free_source(shaders[3].source);

    ct_rh_free_applier(shaders[0].applier);
    ct_rh_free_applier(shaders[1].applier);
    ct_rh_free_applier(shaders[2].applier);
    ct_rh_free_applier(shaders[3].applier);

    ct_rb_freeRGB(tex);

    ct_ca_aNamePop();
    return 0;
}
