#include <stdlib.h>

#include "camera.h"
#include "lib/common/triangle.h"



void ct_rc_initCam(ct_rc_Camera *cam, ct_cv_Vector *pos, ct_cv_Vector *rotXYZ, double fov) {
    if (pos) {
        cam->pos.x = FXDIV(pos->x, pos->w);
        cam->pos.y = FXDIV(pos->y, pos->w);
        cam->pos.z = FXDIV(pos->z, pos->w);
        cam->pos.w = FX1P;
    }

    else {
        cam->pos.x = 0;
        cam->pos.y = 0;
        cam->pos.z = 0;
        cam->pos.w = FX1P;
    }

    ct_rc_setFov(cam, fov);

    // there is probably a more efficient way, but this
    // is camera initialization, so it is probably not
    // a very good idea to bother too much with converting
    // yaw-pitch-row to axis-angle beforehand
    ct_rc_rotateToXYZ(cam, rotXYZ);
}

void ct_rc_setFov(ct_rc_Camera *cam, double fov) {
    cam->fov = tan(fov / 2);
}

ct_rc_Camera *ct_rc_allocCam(ct_cv_Vector *pos, ct_cv_Vector *rotXYZ, double fov) {
    ct_rc_Camera *cam = malloc(sizeof(ct_rc_Camera));
    ct_rc_initCam(cam, pos, rotXYZ, fov);

    return cam;
}

void ct_rc_freeCam(ct_rc_Camera *heapCam) {
    free(heapCam);
}

void ct_rc_rotateToXYZ(ct_rc_Camera *cam, ct_cv_Vector *rotZYX) {
    ct_cq_initQuaternion(&cam->rot);

    ct_rc_rotateByXYZ(cam, rotZYX);
}

void ct_rc_rotateByXYZ(ct_rc_Camera *cam, ct_cv_Vector *rotXYZ) {
    ct_cq_addRotOnX(&cam->rot, -rotXYZ->x);
    ct_cq_addRotOnY(&cam->rot, -rotXYZ->y);
    ct_cq_addRotOnZ(&cam->rot, -rotXYZ->z);
}

void ct_rc_rotateByQuat(ct_rc_Camera *cam, ct_cq_Quaternion *rotQuat) {
    ct_cq_multiply(&cam->rot, rotQuat);
}

void ct_rc_applyPerspective(ct_cv_Vector *targ, ct_rc_Camera *cam) {
    ct_cv_subFrom(targ, &cam->pos);
    ct_cq_applyRotTo(targ, &cam->rot);
    
    fixed_t dist = targ->z;

    ct_cv_fMulTo(targ, cam->fov / dist);

    targ->z = dist; // to use for Z-buffering etc
}

void ct_rc_makePerspectiveTriangle(ct_ct_Triangle2D *targ, ct_rc_Camera *cam, ct_ct_Triangle3D *tri) {
    targ->ua = tri->ua;
    targ->ub = tri->ub;
    targ->uc = tri->uc;

    targ->va = tri->va;
    targ->vb = tri->vb;
    targ->vc = tri->vc;

    targ->shaders    = tri->shaders;
    targ->numShaders = tri->numShaders;

    ct_rc_applyPerspective(&targ->a, cam);
    ct_rc_applyPerspective(&targ->b, cam);
    ct_rc_applyPerspective(&targ->c, cam);
}
