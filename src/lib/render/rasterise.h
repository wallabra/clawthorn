#ifndef HG_RASTERISE
#define HG_RASTERISE

#include "lib/common/triangle.h"
#include "lib/common/vector.h"
#include "surface.h"



typedef struct ct_rr_Rasterisation_s {
    long bigside;
    double *blockDepths;
    ct_rs_Surface *surf;
    
} ct_rr_Rasterisation;


void ct_rr_clearDepths(ct_rr_Rasterisation *targ);

long convertX(ct_rr_Rasterisation *targ, double x);
long convertY(ct_rr_Rasterisation *targ, double y);
double unconvertX(ct_rr_Rasterisation *targ, long x);
double unconvertY(ct_rr_Rasterisation *targ, long y);

void ct_rr_init_raster(ct_rr_Rasterisation *targ, ct_rs_Surface *surf);
void ct_rr_deinit_raster(ct_rr_Rasterisation *stackRaster);
void ct_rr_free_raster(ct_rr_Rasterisation *heapRaster);

long ct_rr_rasterizeInto(ct_rr_Rasterisation *targ, ct_ct_Triangle2D *tri);



#endif
