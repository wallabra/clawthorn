#ifndef HG_SURFACE
#define HG_SURFACE


#include "bitmap.h"
#include "window.h"



struct ct_rh_ShaderEffect_s;

typedef struct ct_rs_Color_s {
    double r;
    double g;
    double b;
} ct_rs_Color;


typedef struct ct_rs_Surface_s {
    long width;
    long height;
    void *data;

    void (*setPixel)(struct ct_rs_Surface_s *surf, long x, long y, ct_rs_Color *color);
    void (*clear)(struct ct_rs_Surface_s *surf);
    ct_rs_Color *(*getPixel)(struct ct_rs_Surface_s *surf, long x, long y);
    ct_rs_Color (*getPixelValue)(struct ct_rs_Surface_s *surf, long x, long y);
    
} ct_rs_Surface;

ct_rs_Color *ct_rs_color_make(double r, double g, double b);
ct_rs_Color ct_rs_color_value(double r, double g, double b);
ct_rs_Color *ct_rs_color_black(void);
ct_rs_Color *ct_rs_color_white(void);
void ct_rs_color_free(ct_rs_Color *col);
void ct_rs_checkColor(ct_rs_Color *color);


// == Surface creation utilities ==

ct_rs_Surface *ct_rs_surf_new(void);
void ct_rs_surf_free(ct_rs_Surface *surf);

void ct_rs_surf_init_bitmap_rgb(ct_rs_Surface *surf, ct_rb_RGBBitmap *bitmap);
void ct_rs_surf_init_bitmap_grey(ct_rs_Surface *surf, ct_rb_DoubleBitmap *bitmap);
void ct_rs_surf_init_window(ct_rs_Surface *surf, ct_rw_SDLApp *app);
void ct_rs_surf_clear(ct_rs_Surface *surf);


// == Pixel manipulation utilities ==

void ct_rs_surf_pixel_add(ct_rs_Surface *surf, long x, long y, double r, double g, double b);
void ct_rs_surf_pixel_sub(ct_rs_Surface *surf, long x, long y, double r, double g, double b);
void ct_rs_surf_pixel_mul(ct_rs_Surface *surf, long x, long y, double r, double g, double b);
void ct_rs_surf_pixel_blend(ct_rs_Surface *surf, long x, long y, double r, double g, double b, double weight);

#include "shader.h"



#endif
