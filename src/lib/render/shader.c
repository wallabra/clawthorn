#include "shader.h"
#include "surface.h"
#include "rasterise.h"


void _ct_rh_apply_normal(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor) {
    //(*surf->setPixel)(surf, x, y, sourceColor);
    *targetColor = *sourceColor;
}

void _ct_rh_apply_blend(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor) {
    //ct_rs_surf_pixel_blend(surf, x, y, sourceColor->r, sourceColor->g, sourceColor->b, *(double*)(applier->data));

    double weight = *(double *)(applier->data);

    double targWeight = (1. - weight);

    targetColor->r = weight * sourceColor->r + targWeight * targetColor->r;
    targetColor->g = weight * sourceColor->g + targWeight * targetColor->g;
    targetColor->b = weight * sourceColor->b + targWeight * targetColor->b;
}

void _ct_rh_apply_add(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor) {
    targetColor->r += sourceColor->r;
    targetColor->g += sourceColor->g;
    targetColor->b += sourceColor->b;
}

void _ct_rh_apply_mul(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor) {
    targetColor->r *= sourceColor->r;
    targetColor->g *= sourceColor->g;
    targetColor->b *= sourceColor->b;
}

void _ct_rh_apply_sub(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor) {
    targetColor->r -= sourceColor->r;
    targetColor->g -= sourceColor->g;
    targetColor->b -= sourceColor->b;
}

//---------//

ct_rh_ShaderApplier *ct_rh_normal_applier() {
    ct_rh_ShaderApplier *res = malloc(sizeof(ct_rh_ShaderApplier));

    res->apply = &_ct_rh_apply_normal;
    res->data = NULL;

    return res;
}

ct_rh_ShaderApplier *ct_rh_blend_applier(double weight) {
    ct_rh_ShaderApplier *res = malloc(sizeof(ct_rh_ShaderApplier));

    double *data = malloc(sizeof(double));
    *data = weight;

    res->apply = &_ct_rh_apply_blend;
    res->data = data;

    return res;
}

ct_rh_ShaderApplier *ct_rh_add_applier() {
    ct_rh_ShaderApplier *res = malloc(sizeof(ct_rh_ShaderApplier));

    res->apply = &_ct_rh_apply_add;
    res->data = NULL;

    return res;
}

ct_rh_ShaderApplier *ct_rh_mul_applier() {
    ct_rh_ShaderApplier *res = malloc(sizeof(ct_rh_ShaderApplier));

    res->apply = &_ct_rh_apply_mul;
    res->data = NULL;

    return res;
}

ct_rh_ShaderApplier *ct_rh_sub_applier() {
    ct_rh_ShaderApplier *res = malloc(sizeof(ct_rh_ShaderApplier));

    res->apply = &_ct_rh_apply_sub;
    res->data = NULL;

    return res;
}

void ct_rh_free_applier(ct_rh_ShaderApplier *applier) {
    if (applier->data) free(applier->data);

    free(applier);
}


//==== SOURCES ====


ct_rs_Color _ct_rh_colorsource_texture(struct ct_rh_ShaderSource_s *source, ct_ct_Triangle2D *tri, long x, long y, double px, double py, double baryA, double baryB, double baryC, double depth) {
    ct_rs_Color res;

    ct_rb_RGBBitmap *tex = source->dataPtr;

    double u = tri->ua * baryA + tri->ub * baryB + tri->uc * baryC;
    double v = tri->va * baryA + tri->vb * baryB + tri->vc * baryC;

    long upx = u * tex->width;
    long vpx = v * tex->height;

    long ind = (vpx * tex->width + upx) * 3;

    res.r = tex->vals[ind++];
    res.g = tex->vals[ind++];
    res.b = tex->vals[ind];

    return res;
}

ct_rs_Color _ct_rh_colorsource_bary(struct ct_rh_ShaderSource_s *source, ct_ct_Triangle2D *tri, long x, long y, double px, double py, double baryA, double baryB, double baryC, double depth) {
    ct_rs_Color res;

    res.r = baryA;
    res.g = baryB;
    res.b = baryC;

    return res;
}

ct_rs_Color _ct_rh_colorsource_color(struct ct_rh_ShaderSource_s *source, ct_ct_Triangle2D *tri, long x, long y, double px, double py, double baryA, double baryB, double baryC, double depth) {
    ct_rs_Color res;
    res = *(ct_rs_Color*)(source->dataPtr);

    return res;
}

//---------//

ct_rh_ShaderSource *ct_rh_texture_source(ct_rb_RGBBitmap *tex) {
    ct_rh_ShaderSource *res = malloc(sizeof(ct_rh_ShaderSource));
    
    res->colorFor = &_ct_rh_colorsource_texture;
    res->dataPtr = tex;

    res->data1 = NULL;
    res->data2 = NULL;

    return res;
}

ct_rh_ShaderSource *ct_rh_color_source(ct_rs_Color *color) {
    ct_rh_ShaderSource *res = malloc(sizeof(ct_rh_ShaderSource));

    res->colorFor = &_ct_rh_colorsource_color;
    res->dataPtr = color;

    res->data1 = NULL;
    res->data2 = NULL;

    return res;
}

ct_rh_ShaderSource *ct_rh_barycentric_source() {
    ct_rh_ShaderSource *res = malloc(sizeof(ct_rh_ShaderSource));
    
    res->colorFor = &_ct_rh_colorsource_bary;

    res->data1 = NULL;
    res->data2 = NULL;

    return res;
}

void ct_rh_free_source(ct_rh_ShaderSource *source) {
    if (source->data1) free(source->data1);
    if (source->data2) free(source->data2);

    free(source);
}
