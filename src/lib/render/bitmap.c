#include <png.h>
#include <string.h>
#include <stdint.h>
#include <assert.h>

#include "bitmap.h"
#include "surface.h"
#include "lib/common/abort.h"



ct_rb_DoubleBitmap *ct_rb_newD(short width, long height) {
    double *vals = malloc(sizeof(double) * width * height);
    memset(vals, 0., sizeof(double) * width * height);

    ct_rb_DoubleBitmap *res = malloc(sizeof(ct_rb_DoubleBitmap));

    res->vals = vals;
    res->width = width;
    res->height = height;
    
    return res;
}

ct_rb_RGBBitmap *ct_rb_newRGB(short width, long height) {
    double *vals = malloc(sizeof(double) * width * height * 3);
    memset(vals, 0, sizeof(double) * width * height * 3);

    ct_rb_RGBBitmap *res = malloc(sizeof(ct_rb_RGBBitmap));

    res->vals = vals;
    res->width = width;
    res->height = height;
    
    return res;
}

void ct_rb_clearGrey(ct_rb_DoubleBitmap *bitmap) {
    memset(bitmap->vals, 0, sizeof(double) * bitmap->width * bitmap->height);
}

void ct_rb_clearRGB(ct_rb_RGBBitmap *bitmap) {
    memset(bitmap->vals, 0, sizeof(double) * bitmap->width * bitmap->height * 3);
}

void ct_rb_freeD(ct_rb_DoubleBitmap *bitmap) {
    free(bitmap->vals);
    free(bitmap);
}


void ct_rb_freeRGB(ct_rb_RGBBitmap *bitmap) {
    free(bitmap->vals);
    free(bitmap);
}

void ct_rb_exportGreyscale(FILE *fp, ct_rb_DoubleBitmap *bitmap) {
    png_structp sptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!sptr) ct_ca_doAbort("[ct_rb_exportGreyscale] png_create_write_struct failed");

    png_infop iptr = png_create_info_struct(sptr);
    if (!iptr) ct_ca_doAbort("[ct_rb_exportGreyscale] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportGreyscale] png_init_io failed");

    png_init_io(sptr, fp);
    
    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportGreyscale] writing png header failed");
    png_set_IHDR(
        sptr, iptr,
        bitmap->width, bitmap->height, 16, PNG_COLOR_TYPE_GRAY,
        PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE
    );
    png_write_info(sptr, iptr);

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportGreyscale] writing data");

    // convert doubles to ints
    uint8_t **out = malloc(sizeof(uint8_t *) * bitmap->height);

    for (long y = 0; y < bitmap->height; y++) {
        uint8_t *row = malloc(sizeof(short) * bitmap->width);
        out[y] = row;
        long si = y * bitmap->width;

        for (long x = 0; x < bitmap->width; x++) {
            long xi = x * 2;
            short val = (short) (bitmap->vals[si + x] * USHRT_MAX);

            row[xi + 0] = val & 0xFF00 >> 8;
            row[xi + 1] = val & 0x00FF;
        }
    }

    png_write_image(sptr, (png_bytepp) out);

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportGreyscale] error finishing write");

    png_write_end(sptr, iptr);
    
    // dealloc
    for (long y = 0; y < bitmap->height; y++) {
        free(out[y]);
    }
    
    free(out);
}

void ct_rb_exportRGB(FILE *fp, ct_rb_RGBBitmap *bitmap) {
    png_structp sptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!sptr) ct_ca_doAbort("[ct_rb_exportRGB] png_create_write_struct failed");

    png_infop iptr = png_create_info_struct(sptr);
    if (!iptr) ct_ca_doAbort("[ct_rb_exportRGB] png_create_info_struct failed");

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportRGB] png_init_io failed");

    png_init_io(sptr, fp);
    
    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportRGB] writing png header failed");
    png_set_IHDR(
        sptr, iptr,
        bitmap->width, bitmap->height, 16, PNG_COLOR_TYPE_RGB,
        PNG_INTERLACE_NONE, PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE
    );
    png_write_info(sptr, iptr);

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportRGB] writing data");

    // convert doubles to ints
    uint8_t **out = malloc(sizeof(uint8_t *) * bitmap->height);

    for (long y = 0; y < bitmap->height; y++) {
        uint8_t *row = malloc(sizeof(short) * bitmap->width * 6);
        long si = y * bitmap->width * 3;

        for (long x = 0; x < bitmap->width; x++) {
            long xi = x * 3;

            short r = (uint16_t) (bitmap->vals[si + xi + 0] * USHRT_MAX);
            short g = (uint16_t) (bitmap->vals[si + xi + 1] * USHRT_MAX);
            short b = (uint16_t) (bitmap->vals[si + xi + 2] * USHRT_MAX);

            row[xi * 2]      = r;
            row[xi * 2 + 1]  = r >> 8;
            row[xi * 2 + 2]  = g;
            row[xi * 2 + 3]  = g >> 8;
            row[xi * 2 + 4]  = b;
            row[xi * 2 + 5]  = b >> 8;
        }

        out[y] = row;
    }

    png_write_image(sptr, (png_bytepp) out);

    if (setjmp(png_jmpbuf(sptr))) ct_ca_doAbort("[ct_rb_exportRGB] error finishing write");

    png_write_end(sptr, iptr);
    
    // dealloc
    for (long y = 0; y < bitmap->height; y++) {
        free(out[y]);
    }
    
    free(out);
}


ct_rb_RGBBitmap *ct_rb_pngLoadRGB(FILE *fp, char *nameForTroubleshooting) {
    ct_ca_aNamePush("ct_rb_pngLoadRGB");

    png_structp png = png_create_read_struct(PNG_LIBPNG_VER_STRING, 0, 0, 0);
    if (!png) ct_ca_doAbort("png_create_read_struct failed");

    png_infop info = png_create_info_struct(png);
    if (!info) ct_ca_doAbort("png_create_info_struct failed");

    if (setjmp(png_jmpbuf(png))) {
        if (nameForTroubleshooting)
            ct_ca_doAbort("libpng issue reading PNG file %s", nameForTroubleshooting);

        else
            ct_ca_doAbort("libpng issue reading some PNG file");
    }

    png_init_io(png, fp);
    png_read_info(png, info);

    uint32_t width = png_get_image_width(png, info);
    uint32_t height = png_get_image_height(png, info);
    png_byte colorType = png_get_color_type(png, info);
    png_byte bitDepth = png_get_bit_depth(png, info);

    if (colorType == PNG_COLOR_TYPE_PALETTE)
        png_set_palette_to_rgb(png);

    else if (colorType == PNG_COLOR_TYPE_RGBA || colorType == PNG_COLOR_TYPE_RGB_ALPHA)
        png_set_strip_alpha(png);

    else if (colorType == PNG_COLOR_TYPE_GRAY) {
        // png_set_gray_to_rgb wants 8-bit gray
        if (bitDepth < 8) {
            png_set_expand_gray_1_2_4_to_8(png);
            bitDepth = 8;
        }

        png_set_gray_to_rgb(png);
    }

    if (bitDepth < 16) {
        png_set_scale_16(png);
        bitDepth = 16;
    }

    png_read_update_info(png, info);

    // png_set_swap is not called - the swap is done manually, see the lines right below
    // the declaration of bptr.

    png_bytepp rowPointers = malloc(sizeof(png_bytep) * height);

    for (long y = 0; y < height; y++) {
        rowPointers[y] = (png_bytep) malloc(width * sizeof(uint16_t) * 3);
    }

    png_read_image(png, rowPointers);

    // pass rows to bitmap
    ct_rb_RGBBitmap *bitmap = ct_rb_newRGB(width, height);

    //   (defining loop variables here might be faster)
    long yi;
    long i;

    png_bytep bptr;
    uint16_t r;
    uint16_t g;
    uint16_t b;

    for (long y = 0; y < height; y++) {
        yi = y * width * 3;

        for (long x = 0; x < width; x++) {
            i  = yi + x * 3;

            bptr = rowPointers[y] + x * 3;

            r = (uint16_t)(bptr[0]) << 8 | bptr[1];
            g = (uint16_t)(bptr[2]) << 8 | bptr[3];
            b = (uint16_t)(bptr[4]) << 8 | bptr[5];

            bitmap->vals[i]        = (double)(r) / USHRT_MAX;
            bitmap->vals[i + 1]    = (double)(g) / USHRT_MAX;
            bitmap->vals[i + 2]    = (double)(b) / USHRT_MAX;
        }
    }

    ct_ca_aNamePop();

    for (long y = 0; y < height; y++) {
        free(rowPointers[y]);
    }

    free(rowPointers);

    png_read_end(png, info);

    return bitmap;
}


void ct_rb_setpix_rgb(ct_rb_RGBBitmap *bitmap, long x, long y, struct ct_rs_Color_s *col) {
    long i = (y * bitmap->width + x) * 3;

    assert(x < bitmap->width && y < bitmap->height);

    bitmap->vals[i]     = col->r;
    bitmap->vals[i + 1] = col->g;
    bitmap->vals[i + 2] = col->b;
}

void ct_rb_getpix_rgb(ct_rb_RGBBitmap *bitmap, long x, long y, struct ct_rs_Color_s *col) {
    long i = (y * bitmap->width + x) * 3;

    assert(x < bitmap->width && y < bitmap->height);

    col->r = bitmap->vals[i];
    col->g = bitmap->vals[i + 1];
    col->b = bitmap->vals[i + 2];
}