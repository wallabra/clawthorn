#include <limits.h>
#include <stdlib.h>

#include "surface.h"



ct_rs_Color *ct_rs_color_make(double r, double g, double b) {
    ct_rs_Color *res = malloc(sizeof(ct_rs_Color));

    res->r = r;
    res->g = g;
    res->b = b;

    return res;
}

ct_rs_Color ct_rs_color_value(double r, double g, double b) {
    ct_rs_Color res;

    res.r = r;
    res.g = g;
    res.b = b;

    return res;
}

ct_rs_Color *ct_rs_color_black(void) {
    ct_rs_Color *res = malloc(sizeof(ct_rs_Color));

    res->r = 0.;
    res->g = 0.;
    res->b = 0.;

    return res;
}

ct_rs_Color *ct_rs_color_white(void) {
    ct_rs_Color *res = malloc(sizeof(ct_rs_Color));

    res->r = 1.;
    res->g = 1.;
    res->b = 1.;

    return res;
}

void ct_rs_color_free(ct_rs_Color *col) {
    free(col);
}


// == Pixel setters and getters ==


ct_rs_Color *_ct_rs_bitmap_rgb_px_directGet(ct_rb_RGBBitmap *bitmap, long x, long y) {
    long i = (y * bitmap->width + x) * 3;

    return ct_rs_color_make(bitmap->vals[i], bitmap->vals[i + 1], bitmap->vals[i + 2]);
}

ct_rs_Color _ct_rs_bitmap_rgb_px_directGetValue(ct_rb_RGBBitmap *bitmap, long x, long y) {
    long i = (y * bitmap->width + x) * 3;

    return ct_rs_color_value(bitmap->vals[i], bitmap->vals[i + 1], bitmap->vals[i + 2]);
}

void _ct_rs_bitmap_rgb_px_directSet(ct_rb_RGBBitmap *bitmap, long x, long y, ct_rs_Color *color) {
    long i = (y * bitmap->width + x) * 3;

    bitmap->vals[(i++)]       = color->r;
    bitmap->vals[(i++)]       = color->g;
    bitmap->vals[i]           = color->b;
}

ct_rs_Color *_ct_rs_bitmap_rgb_px_get(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rb_RGBBitmap *bitmap = (ct_rb_RGBBitmap *) surf->data;
    
    return _ct_rs_bitmap_rgb_px_directGet(bitmap, x, y);
}

ct_rs_Color *_ct_rs_bitmap_grey_px_get(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rb_DoubleBitmap *bitmap = (ct_rb_DoubleBitmap *) surf->data;
    
    long i = y * surf->width + x;

    return ct_rs_color_make(bitmap->vals[i], bitmap->vals[i], bitmap->vals[i]);
}

ct_rs_Color _ct_rs_bitmap_rgb_px_getValue(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rb_RGBBitmap *bitmap = (ct_rb_RGBBitmap *) surf->data;
    
    return _ct_rs_bitmap_rgb_px_directGetValue(bitmap, x, y);
}

ct_rs_Color *_ct_rs_window_px_get(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rw_SDLApp *app = (ct_rw_SDLApp *) surf->data;
    
    return _ct_rs_bitmap_rgb_px_directGet(app->pixelBuffer, x, y);
}

ct_rs_Color _ct_rs_bitmap_grey_px_getValue(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rb_DoubleBitmap *bitmap = (ct_rb_DoubleBitmap *) surf->data;
    
    long i = y * surf->width + x;

    return ct_rs_color_value(bitmap->vals[i], bitmap->vals[i], bitmap->vals[i]);
}

ct_rs_Color _ct_rs_window_px_getValue(struct ct_rs_Surface_s *surf, long x, long y) {
    ct_rw_SDLApp *app = (ct_rw_SDLApp *) surf->data;
    
    return _ct_rs_bitmap_rgb_px_directGetValue(app->pixelBuffer, x, y);
}

void _ct_rs_bitmap_rgb_px_set(struct ct_rs_Surface_s *surf, long x, long y, ct_rs_Color *color) {
    ct_rb_RGBBitmap *bitmap = (ct_rb_RGBBitmap *) surf->data;
    
    long i = (y * surf->width + x) * 3;

    bitmap->vals[(i++)]       = color->r;
    bitmap->vals[(i++)]       = color->g;
    bitmap->vals[i]           = color->b;
}

void _ct_rs_bitmap_grey_px_set(struct ct_rs_Surface_s *surf, long x, long y, ct_rs_Color *color) {
    ct_rb_DoubleBitmap *bitmap = (ct_rb_DoubleBitmap *) surf->data;
    
    long i = y * surf->width + x;

    // too lazy to get the luminance weights right now...
    bitmap->vals[i]         = (color->r + color->g + color->b) / 3;
}

void _ct_rs_window_px_set(struct ct_rs_Surface_s *surf, long x, long y, ct_rs_Color *color) {
    ct_rw_SDLApp *app = (ct_rw_SDLApp *) surf->data;
    
    _ct_rs_bitmap_rgb_px_directSet(app->pixelBuffer, x, y, color);

    char r_byte = UCHAR_MAX * color->r;
    char g_byte = UCHAR_MAX * color->g;
    char b_byte = UCHAR_MAX * color->b;

    SDL_SetRenderDrawColor(app->renderer, r_byte, g_byte, b_byte, 255);
    SDL_RenderDrawPoint(app->renderer, x, app->pixelBuffer->height - y);
}

void _ct_rs_bitmap_rgb_clear(struct ct_rs_Surface_s *surf) {
    ct_rb_DoubleBitmap *bitmap = (ct_rb_DoubleBitmap *) surf->data;

    memset(bitmap->vals, 0, sizeof(double) * surf->width * surf->height * 3);
}

void _ct_rs_bitmap_grey_clear(struct ct_rs_Surface_s *surf) {
    ct_rb_DoubleBitmap *bitmap = (ct_rb_DoubleBitmap *) surf->data;

    memset(bitmap->vals, 0, sizeof(double) * surf->width * surf->height);
}

void _ct_rs_window_clear(struct ct_rs_Surface_s *surf) {
    ct_rw_SDLApp *app = (ct_rw_SDLApp *) surf->data;

    ct_rw_clearColor(app);

    memset(app->pixelBuffer->vals, 0, sizeof(double) * surf->width * surf->height * 3);
}


// == Surface creation utilities ==


ct_rs_Surface *ct_rs_surf_new(void) {
    ct_rs_Surface *res = malloc(sizeof(ct_rs_Surface));

    res->width = 0;
    res->height = 0;

    res->setPixel = 0;
    res->getPixel = 0;

    return res;
}

void ct_rs_surf_free(ct_rs_Surface *surf) {
    free(surf);
}

void ct_rs_surf_init_bitmap_rgb(ct_rs_Surface *surf, ct_rb_RGBBitmap *bitmap) {
    surf->width = bitmap->width;
    surf->height = bitmap->height;

    surf->getPixel = &_ct_rs_bitmap_rgb_px_get;
    surf->setPixel = &_ct_rs_bitmap_rgb_px_set;
    surf->getPixelValue = &_ct_rs_bitmap_rgb_px_getValue;
    surf->clear = &_ct_rs_bitmap_rgb_clear;
    
    surf->data = (void *) bitmap;
}

void ct_rs_surf_init_bitmap_grey(ct_rs_Surface *surf, ct_rb_DoubleBitmap *bitmap) {
    surf->width = bitmap->width;
    surf->height = bitmap->height;

    surf->getPixel = &_ct_rs_bitmap_grey_px_get;
    surf->setPixel = &_ct_rs_bitmap_grey_px_set;
    surf->getPixelValue = &_ct_rs_bitmap_grey_px_getValue;
    surf->clear = &_ct_rs_bitmap_grey_clear;

    surf->data = (void *) bitmap;
}

void ct_rs_surf_init_window(ct_rs_Surface *surf, ct_rw_SDLApp *app) {
    surf->width = app->pixelBuffer->width;
    surf->height = app->pixelBuffer->height;

    surf->getPixel = &_ct_rs_window_px_get;
    surf->setPixel = &_ct_rs_window_px_set;
    surf->getPixelValue = &_ct_rs_window_px_getValue;
    surf->clear = &_ct_rs_window_clear;

    surf->data = (void *) app;
}

void ct_rs_surf_clear(ct_rs_Surface *surf) {
    (*surf->clear)(surf);
}

void ct_rs_checkColor(ct_rs_Color *color) {
    if (color->r > 1) color->r = 1;
    if (color->g > 1) color->g = 1;
    if (color->b > 1) color->b = 1;

    if (color->r < 0) color->r = 0;
    if (color->g < 0) color->g = 0;
    if (color->b < 0) color->b = 0;
}

void ct_rs_surf_pixel_add(ct_rs_Surface *surf, long x, long y, double r, double g, double b) {
    ct_rs_Color *color = (*surf->getPixel)(surf, x, y);

    color->r += r;
    color->g += g;
    color->b += b;

    ct_rs_checkColor(color);

    (*surf->setPixel)(surf, x, y, color);

    free(color);
}

void ct_rs_surf_pixel_sub(ct_rs_Surface *surf, long x, long y, double r, double g, double b) {
    ct_rs_Color *color = (*surf->getPixel)(surf, x, y);

    color->r -= r;
    color->g -= g;
    color->b -= b;

    ct_rs_checkColor(color);

    (*surf->setPixel)(surf, x, y, color);

    free(color);
}

void ct_rs_surf_pixel_mul(ct_rs_Surface *surf, long x, long y, double r, double g, double b) {
    ct_rs_Color *color = (*surf->getPixel)(surf, x, y);

    color->r *= r;
    color->g *= g;
    color->b *= b;

    ct_rs_checkColor(color);

    (*surf->setPixel)(surf, x, y, color);

    free(color);
}

void ct_rs_surf_pixel_blend(ct_rs_Surface *surf, long x, long y, double r, double g, double b, double weight) {
    ct_rs_Color *color = (*surf->getPixel)(surf, x, y);

    color->r *= 1. - weight;
    color->g *= 1. - weight;
    color->b *= 1. - weight;

    color->r += r * weight;
    color->g += g * weight;
    color->b += b * weight;

    (*surf->setPixel)(surf, x, y, color);

    free(color);
}

#include "rasterise.h"

