#ifndef HG_SHADER
#define HG_SHADER

#include "lib/common/triangle.h"
#include "bitmap.h"
#include "surface.h"



struct ct_rr_Rasterisation_s;

typedef struct ct_rh_ShaderApplier_s {
    void (*apply)(struct ct_rh_ShaderApplier_s *applier, long x, long y, double px, double py, ct_rs_Color *sourceColor, ct_rs_Color *targetColor);
    void *data;     // is freed
    
} ct_rh_ShaderApplier;

typedef struct ct_rh_ShaderSource_s {
    ct_rs_Color (*colorFor)(struct ct_rh_ShaderSource_s *source, ct_ct_Triangle2D *tri, long x, long y, double px, double py, double baryA, double baryB, double baryC, double depth);
    void *data1;    // is freed
    void *data2;    // is freed
    void *dataPtr;  // is not freed
    
} ct_rh_ShaderSource;

typedef struct {
    ct_rh_ShaderSource  *source;
    ct_rh_ShaderApplier *applier;
} ct_rh_Shader;

typedef struct ct_rh_ShaderEffect_s {
    void (*transform)(struct ct_rh_ShaderEffect_s *effect, struct ct_rs_Surface_s *targ, long x, long y, ct_rb_RGBBitmap *frame, ct_rs_Color *color);
    void (*init)(struct ct_rh_ShaderEffect_s *effect, struct ct_rs_Surface_s *targ, ct_rb_RGBBitmap *frame);

    void *data1;    // is freed
    void *data2;    // is freed
    void *dataPtr;  // is not freed

} ct_rh_ShaderEffect;



ct_rh_ShaderApplier *ct_rh_normal_applier();
ct_rh_ShaderApplier *ct_rh_blend_applier(double weight);
ct_rh_ShaderApplier *ct_rh_add_applier();
ct_rh_ShaderApplier *ct_rh_mul_applier();
ct_rh_ShaderApplier *ct_rh_sub_applier();

void ct_rh_free_applier(ct_rh_ShaderApplier *applier);

ct_rh_ShaderSource *ct_rh_texture_source(ct_rb_RGBBitmap *tex);
ct_rh_ShaderSource *ct_rh_color_source(ct_rs_Color *color);
ct_rh_ShaderSource *ct_rh_barycentric_source();

void ct_rh_free_source(ct_rh_ShaderSource *source);


#endif
