#ifndef HG_TRIANGLE
#define HG_TRIANGLE


#include <stdint.h>

#include "vector.h"



struct ct_rh_Shader_s;

typedef struct {
    ct_cv_Vector a;
    ct_cv_Vector b;
    ct_cv_Vector c;

    uint8_t numShaders;
    struct ct_rh_Shader_s **shaders;
    
    long ua;
    long ub;
    long uc;

    long va;
    long vb;
    long vc;
    
} ct_ct_Triangle;

typedef ct_ct_Triangle ct_ct_Triangle3D;
typedef ct_ct_Triangle ct_ct_Triangle2D;

ct_ct_Triangle *ct_ct_make(ct_cv_Vector *a, ct_cv_Vector *b, ct_cv_Vector *c);
void ct_ct_init(ct_ct_Triangle *tri, ct_cv_Vector a, ct_cv_Vector b, ct_cv_Vector c);
void ct_ct_free(ct_ct_Triangle *tri);

#include "lib/render/shader.h"



#endif
