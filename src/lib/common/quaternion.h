#ifndef HG_QUATERNION
#define HG_QUATERNION

#include "vector.h"
#include "fixed.h"



typedef struct {
    fixed_t r; // n
    fixed_t i; // n * i
    fixed_t j; // n * j
    fixed_t k; // n * k
} ct_cq_Quaternion;


void ct_cq_initQuaternion(ct_cq_Quaternion *quat);

void ct_cq_conjugate(ct_cq_Quaternion *q);
void ct_cq_multiply(ct_cq_Quaternion *targ, ct_cq_Quaternion *quat);

void ct_cq_applyRotTo(ct_cv_Vector *targ, ct_cq_Quaternion *quat);
ct_cv_Vector ct_cq_applyRotVal(ct_cv_Vector *vec, ct_cq_Quaternion *quat);

void ct_cq_setRotOnAxis(ct_cq_Quaternion *targ, ct_cv_Vector *axis, fixed_t angle);
void ct_cq_setRotOnAxisF(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angle);
void ct_cq_addRotOnAxis(ct_cq_Quaternion *targ, ct_cv_Vector *axis, fixed_t angle);
void ct_cq_addRotOnAxisF(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angle);

void ct_cq_addRotOnX(ct_cq_Quaternion *targ, fixed_t pitch);
void ct_cq_addRotOnY(ct_cq_Quaternion *targ, fixed_t roll);
void ct_cq_addRotOnZ(ct_cq_Quaternion *targ, fixed_t yaw);

extern const ct_cq_Quaternion ct_cq_unitQuat;
extern const ct_cq_Quaternion ct_cq_zeroQuat;



#endif