#include "quaternion.h"
#include "fixed.h"
#include "math.h"



extern const ct_cq_Quaternion ct_cq_unitQuat = {
    FX1P,   // r (real component)
    0,      // i
    0,      // j
    0,      // k
};

extern const ct_cq_Quaternion ct_cq_zeroQuat = {
    0,  // r (real component)
    0,  // i
    0,  // j
    0,  // k
};

void ct_cq_initQuaternion(ct_cq_Quaternion *quat) {
    quat->i = 0;
    quat->j = 0;
    quat->k = 0;
    quat->r = 0;
}

void ct_cq_conjugate(ct_cq_Quaternion *q) {
    q->i = -q->i;
    q->j = -q->j;
    q->k = -q->k;
}

void ct_cq_multiply(ct_cq_Quaternion *a, ct_cq_Quaternion *b) {
    a->r =  FXMUL(a->r, b->r) - FXMUL(a->i, b->i) - FXMUL(a->j, b->j) - FXMUL(a->k, b->k);
    a->i =  FXMUL(a->r, b->i) + FXMUL(a->i, b->r) + FXMUL(a->j, b->k) - FXMUL(a->k, b->j);
    a->j =  FXMUL(a->r, b->j) + FXMUL(a->j, b->r) + FXMUL(a->k, b->i) - FXMUL(a->i, b->k);
    a->k =  FXMUL(a->r, b->k) + FXMUL(a->k, b->r) + FXMUL(a->i, b->j) - FXMUL(a->j, b->i);
}

void ct_cq_applyRotTo(ct_cv_Vector *targ, ct_cq_Quaternion *quat) {
    ct_cq_Quaternion vq;

    vq.i = targ->x;
    vq.j = targ->y;
    vq.k = targ->z;
    vq.r = 0;

    ct_cq_Quaternion qconj = *quat;

    ct_cq_conjugate(&qconj);

    ct_cq_Quaternion vrot = *quat;

    ct_cq_multiply(&vrot, &vq);
    ct_cq_multiply(&vrot, &qconj);

    targ->x = qconj.i;
    targ->y = qconj.j;
    targ->z = qconj.k;
}

ct_cv_Vector ct_cq_applyRotVal(ct_cv_Vector *vec, ct_cq_Quaternion *quat) {
    ct_cv_Vector res = *vec;
    ct_cq_applyRotTo(&res, quat);
    
    return res;
}

static const ct_cv_Vector _axisX = { 1, 0, 0, 1 };
static const ct_cv_Vector _axisY = { 0, 1, 0, 1 };
static const ct_cv_Vector _axisZ = { 0, 0, 1, 1 };

void ct_cq_addRotOnX(ct_cq_Quaternion *targ, fixed_t pitch) {
    ct_cq_addRotOnAxis(targ, &_axisX, pitch);
}

void ct_cq_addRotOnY(ct_cq_Quaternion *targ, fixed_t yaw) {
    ct_cq_addRotOnAxis(targ, &_axisY, yaw);
}

void ct_cq_addRotOnZ(ct_cq_Quaternion *targ, fixed_t roll) {
    ct_cq_addRotOnAxis(targ, &_axisZ, roll);
}

static void _ct_cq_setRotOnAxisSC(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angsin, double angcos) {
    // (make sure axis is a normal vector before calling!)

    targ->r = angcos;
    targ->i = angsin * axis->x;
    targ->j = angsin * axis->y;
    targ->k = angsin * axis->z;
}

static void _ct_cq_addRotOnAxisSC(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angsin, double angcos) {
    ct_cq_Quaternion arot;
    _ct_cq_setRotOnAxisSC(&arot, axis, angsin, angcos);
    ct_cq_multiply(targ, &arot);
}

void ct_cq_setRotOnAxis(ct_cq_Quaternion *targ, ct_cv_Vector *axis, fixed_t angle) {
    double angsin = sin(FXTOF(angle));
    double angcos = cos(FXTOF(angle));

    _ct_cq_setRotOnAxisSC(targ, axis, angsin, angcos);
}

void ct_cq_setRotOnAxisF(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angle) {
    double angsin = sin(angle);
    double angcos = cos(angle);

    _ct_cq_setRotOnAxisSC(targ, axis, angsin, angcos);
}

void ct_cq_addRotOnAxis(ct_cq_Quaternion *targ, ct_cv_Vector *axis, fixed_t angle) {
    double angsin = sin(FXTOF(angle));
    double angcos = cos(FXTOF(angle));

    _ct_cq_addRotOnAxisSC(targ, axis, angsin, angcos);
}

void ct_cq_addRotOnAxisF(ct_cq_Quaternion *targ, ct_cv_Vector *axis, double angle) {
    double angsin = sin(angle);
    double angcos = cos(angle);

    _ct_cq_addRotOnAxisSC(targ, axis, angsin, angcos);
}
