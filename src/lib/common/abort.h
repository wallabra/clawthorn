#ifndef HG_ABORT
#define HG_ABORT


void ct_ca_doAbort(const char *msg, ...);
void ct_ca_aNamePush(const char *name);
void ct_ca_aNamePop(void);
char *ct_ca_getAName(void);


#endif
