#ifndef HG_VECTOR
#define HG_VECTOR



//==== VECTORS ====

#define VIND(v,i) (((long*)v)[(i)])

typedef struct ct_cv_Vector {
    long x, y, z;
    long w; // usually SIMD padding
} ct_cv_Vector;


ct_cv_Vector *ct_cv_vZero();
void ct_cv_vFree(ct_cv_Vector *v);
ct_cv_Vector *ct_cv_vNew(long x, long y, long z);
ct_cv_Vector  ct_cv_vVal(long x, long y, long z);
ct_cv_Vector *ct_cv_duplicate(ct_cv_Vector *from);


ct_cv_Vector *ct_cv_add(ct_cv_Vector* a, ct_cv_Vector* b);
void ct_cv_addTo(ct_cv_Vector* targ, ct_cv_Vector* b);
void ct_cv_setAdd(ct_cv_Vector* targ, ct_cv_Vector* a, ct_cv_Vector* b);

ct_cv_Vector *ct_cv_sub(ct_cv_Vector* a, ct_cv_Vector* b);
void ct_cv_subFrom(ct_cv_Vector* targ, ct_cv_Vector* b);
void ct_cv_setSub(ct_cv_Vector* targ, ct_cv_Vector* a, ct_cv_Vector* b);

ct_cv_Vector *ct_cv_vMul(ct_cv_Vector* a, ct_cv_Vector* b);
void ct_cv_vMulTo(ct_cv_Vector* targ, ct_cv_Vector* b);

ct_cv_Vector *ct_cv_fMul(ct_cv_Vector* a, double b);
void ct_cv_fMulTo(ct_cv_Vector* targ, double b);

ct_cv_Vector *ct_cv_vDiv(ct_cv_Vector* a, ct_cv_Vector* b);
void ct_cv_vDivTo(ct_cv_Vector* targ, ct_cv_Vector* b);

ct_cv_Vector *ct_cv_fDiv(ct_cv_Vector* a, double b);
void ct_cv_fDivTo(ct_cv_Vector* targ, double b);

long ct_cv_vSize(ct_cv_Vector *v);


long ct_cv_vDot(ct_cv_Vector* a, ct_cv_Vector* b);

ct_cv_Vector *ct_cv_cross3(ct_cv_Vector *a, ct_cv_Vector *b);
void ct_cv_cross3val(ct_cv_Vector *targ, ct_cv_Vector *a, ct_cv_Vector *b);

long ct_cv_cross2(ct_cv_Vector *a, ct_cv_Vector *b);
double ct_cv_cross2f(ct_cv_Vector *a, ct_cv_Vector *b);


char *ct_cv_vRepr(ct_cv_Vector *v);



#endif
