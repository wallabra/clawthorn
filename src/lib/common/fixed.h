// Fixed point helpers (ditched, floating-point versions kept for compatibility).
#ifndef FIXED_H_GUARD
#define FIXED_H_GUARD


// What once were conversions...
#define FXTOF(fx)  ((double)fx)
#define FTOFX(f)   ((float)f)

// ...are now vestiges...
#define FXMUL(a,b) (a*b)
#define FXDIV(a,b) (a/b)

// ...of wilder dreams.
typedef float fixed_t;

static const fixed_t FX1P =  1.0;
static const fixed_t FX1M = -1.0;

#ifdef M_PI
static const fixed_t FXPI = M_PI;
#else
static const fixed_t FXPI = 3.141592653589793;
#endif


#endif