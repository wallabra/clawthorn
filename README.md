# Clawthorn

Clawthorn is an experimental software 3D rendering engine written in C,
that uses SDL to render frames into a window. It can also export frames
to PNG files using libpng if so desired (edit [main.c](src/main.c) in order
to do so!).

![Example frame](clawthorn-test.gif)
